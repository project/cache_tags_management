<?php

namespace Drupal\cache_tags_management;

use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Interface for ctm_action plugins.
 */
interface CtmActionInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label();

  /**
   * Executes the ctm action.
   *
   * @param \Symfony\Component\HttpFoundation\Response $response
   *   The response.
   */
  public function execute(Response $response);

  /**
   * Builds the configuration form.
   *
   * @param array $config
   *   The config array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The form array.
   */
  public function buildConfigurationForm(array $config, FormStateInterface $form_state);

}
