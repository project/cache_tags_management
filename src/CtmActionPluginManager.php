<?php

namespace Drupal\cache_tags_management;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * CtmAction plugin manager.
 */
class CtmActionPluginManager extends DefaultPluginManager implements CtmActionPluginManagerInterface {

  /**
   * Constructs CtmActionPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/CtmAction',
      $namespaces,
      $module_handler,
      'Drupal\cache_tags_management\CtmActionInterface',
      'Drupal\cache_tags_management\Annotation\CtmAction'
    );
    $this->alterInfo('ctm_action_info');
    $this->setCacheBackend($cache_backend, 'ctm_action_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public function getAction($id) {
    // @todo Implement getAction() method.
  }

  /**
   * {@inheritdoc}
   */
  public function getActions() {
    // @todo Implement getActions() method.
  }

}
