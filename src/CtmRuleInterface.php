<?php

namespace Drupal\cache_tags_management;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a ctm rule entity type.
 */
interface CtmRuleInterface extends ConfigEntityInterface {

}
