<?php

namespace Drupal\cache_tags_management;

use Symfony\Component\HttpFoundation\Response;

/**
 * Interface for managing cache tags.
 */
interface CacheTagsManagementManagerInterface {

  /**
   * Manages the cache tags.
   *
   * @param \Symfony\Component\HttpFoundation\Response $response
   *   The event to process.
   */
  public function manageTags(Response $response);

}
