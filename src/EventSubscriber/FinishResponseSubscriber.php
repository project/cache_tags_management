<?php

namespace Drupal\cache_tags_management\EventSubscriber;

use Drupal\cache_tags_management\CacheTagsManagementManagerInterface;
use Drupal\cache_tags_management\CtmActionPluginManagerInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\Condition\ConditionAccessResolverTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\Context\ContextHandlerInterface;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Drupal\dynamic_page_cache\EventSubscriber\DynamicPageCacheSubscriber;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Response subscriber to clean cache tags.
 */
class FinishResponseSubscriber implements EventSubscriberInterface {

  use ConditionAccessResolverTrait;

  /**
   * The ctm action manager.
   *
   * @var \Drupal\cache_tags_management\CtmActionPluginManagerInterface
   */
  protected $ctmActionManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The context manager service.
   *
   * @var \Drupal\Core\Plugin\Context\ContextRepositoryInterface
   */
  protected $contextRepository;

  /**
   * The context handler.
   *
   * @var \Drupal\Core\Plugin\Context\ContextHandlerInterface
   */
  protected $contextHandler;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * The cache tags management manager.
   *
   * @var \Drupal\cache_tags_management\CacheTagsManagementManagerInterface
   */
  protected $cacheTagsManagementManager;

  /**
   * The renderer configuration.
   *
   * @var array
   */
  protected $rendererConfig;

  /**
   * Constructs a new FinishResponseSubscriber object.
   *
   * @param \Drupal\cache_tags_management\CtmActionPluginManagerInterface $ctm_action_manager
   *   The ctm action manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Plugin\Context\ContextRepositoryInterface $context_repository
   *   The context repository.
   * @param \Drupal\Core\Plugin\Context\ContextHandlerInterface $context_handler
   *   The context handler.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\cache_tags_management\CacheTagsManagementManagerInterface $cache_tags_management_manager
   *   The module handler.
   * @param array $renderer_config
   *   The renderer configuration.
   */
  public function __construct(CtmActionPluginManagerInterface $ctm_action_manager, EntityTypeManagerInterface $entity_type_manager, ContextRepositoryInterface $context_repository, ContextHandlerInterface $context_handler, ModuleHandlerInterface $module_handler, CacheTagsManagementManagerInterface $cache_tags_management_manager, array $renderer_config) {
    $this->ctmActionManager = $ctm_action_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->contextRepository = $context_repository;
    $this->contextHandler = $context_handler;
    $this->moduleHandler = $module_handler;
    $this->cacheTagsManagementManager = $cache_tags_management_manager;
    $this->rendererConfig = $renderer_config;
  }

  /**
   * Registers the methods in this class that should be listeners.
   *
   * @return array
   *   An array of event listener definitions.
   */
  public static function getSubscribedEvents() {
    // Acts just before the Dynamic Page Cache subscriber if is enabled.
    $events[KernelEvents::RESPONSE][] = ['onDynamicRespond', 101];
    return $events;
  }

  /**
   * Sets extra headers on successful responses.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The event to process.
   */
  public function onDynamicRespond(ResponseEvent $event) {
    $response = $event->getResponse();
    if (!$response instanceof CacheableResponseInterface) {
      return;
    }
    // There's no work left to be done if this is a Dynamic Page Cache hit.
    if ($this->moduleHandler->moduleExists('dynamic_page_cache') &&
      $response->headers->get(DynamicPageCacheSubscriber::HEADER) === 'HIT') {
      return;
    }
    $this->cacheTagsManagementManager->manageTags($event->getResponse());
  }

  /**
   * Determines if the response should be cached.
   *
   * @param \Drupal\Core\Cache\CacheableResponseInterface $response
   *   The response to check.
   *
   * @return bool
   *   TRUE if the response should be cached, FALSE otherwise.
   */
  protected function shouldCacheResponse(CacheableResponseInterface $response) {
    $conditions = $this->rendererConfig['auto_placeholder_conditions'];

    $cacheability = $response->getCacheableMetadata();

    // Response's max-age is at or below the configured threshold.
    if ($cacheability->getCacheMaxAge() !== Cache::PERMANENT && $cacheability->getCacheMaxAge() <= $conditions['max-age']) {
      return FALSE;
    }

    // Response has a high-cardinality cache context.
    if (array_intersect($cacheability->getCacheContexts(), $conditions['contexts'])) {
      return FALSE;
    }

    // Response has a high-invalidation frequency cache tag.
    if (array_intersect($cacheability->getCacheTags(), $conditions['tags'])) {
      return FALSE;
    }

    return TRUE;
  }

}
