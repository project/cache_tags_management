<?php

namespace Drupal\cache_tags_management;

use Drupal\Component\Plugin\Exception\ContextException;
use Drupal\Component\Plugin\Exception\MissingValueContextException;
use Drupal\Core\Condition\ConditionAccessResolverTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\Context\ContextHandlerInterface;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Drupal\Core\Plugin\ContextAwarePluginInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Response subscriber to clean cache tags.
 */
class CacheTagsManagementManager implements CacheTagsManagementManagerInterface {

  use ConditionAccessResolverTrait;

  /**
   * The ctm action manager.
   *
   * @var \Drupal\cache_tags_management\CtmActionPluginManagerInterface
   */
  protected $ctmActionManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The context manager service.
   *
   * @var \Drupal\Core\Plugin\Context\ContextRepositoryInterface
   */
  protected $contextRepository;

  /**
   * The context handler.
   *
   * @var \Drupal\Core\Plugin\Context\ContextHandlerInterface
   */
  protected $contextHandler;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * The renderer configuration.
   *
   * @var array
   */
  protected $rendererConfig;

  /**
   * Constructs a new FinishResponseSubscriber object.
   *
   * @param \Drupal\cache_tags_management\CtmActionPluginManagerInterface $ctm_action_manager
   *   The ctm action manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Plugin\Context\ContextRepositoryInterface $context_repository
   *   The context repository.
   * @param \Drupal\Core\Plugin\Context\ContextHandlerInterface $context_handler
   *   The context handler.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param array $renderer_config
   *   The renderer configuration.
   */
  public function __construct(CtmActionPluginManagerInterface $ctm_action_manager, EntityTypeManagerInterface $entity_type_manager, ContextRepositoryInterface $context_repository, ContextHandlerInterface $context_handler, ModuleHandlerInterface $module_handler, array $renderer_config) {
    $this->ctmActionManager = $ctm_action_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->contextRepository = $context_repository;
    $this->contextHandler = $context_handler;
    $this->moduleHandler = $module_handler;
    $this->rendererConfig = $renderer_config;
  }

  /**
   * Manages the cache tags.
   *
   * @param \Symfony\Component\HttpFoundation\Response $response
   *   The event to process.
   */
  public function manageTags(Response $response) {
    $rules = $this->entityTypeManager->getStorage('ctm_rule')->loadMultiple();
    /** @var \Drupal\cache_tags_management\Entity\CtmRule $entity */
    foreach ($rules as $entity) {
      if ($entity->getStatus() === FALSE) {
        continue;
      }
      $conditions = [];
      foreach ($entity->getConditions() as $condition_id => $condition) {
        if ($condition instanceof ContextAwarePluginInterface) {
          try {
            $contexts = $this->contextRepository->getRuntimeContexts(array_values($condition->getContextMapping()));
            $this->contextHandler->applyContextMapping($condition, $contexts);
          }
          catch (MissingValueContextException $e) {
            $missing_value = TRUE;
          }
          catch (ContextException $e) {
            $missing_context = TRUE;
          }
        }
        $conditions[$condition_id] = $condition;
      }

      if ($this->resolveConditions($conditions, $entity->getConditionOperator(TRUE)) !== FALSE) {
        try {
          // Delegate to the plugin.
          $rule_plugin = $entity->getPluginInstance();
          $rule_plugin->execute($response);
        }
        catch (\Exception $e) {
          continue;
        }
      }
    }
  }

}
