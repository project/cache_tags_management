<?php

namespace Drupal\cache_tags_management\Entity;

use Drupal\cache_tags_management\CtmRuleInterface;
use Drupal\Core\Condition\ConditionPluginCollection;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;

/**
 * Defines the ctm rule entity type.
 *
 * @ConfigEntityType(
 *   id = "ctm_rule",
 *   label = @Translation("CTM Rule"),
 *   label_collection = @Translation("CTM Rules"),
 *   label_singular = @Translation("ctm rule"),
 *   label_plural = @Translation("ctm rules"),
 *   label_count = @PluralTranslation(
 *     singular = "@count ctm rule",
 *     plural = "@count ctm rules",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\cache_tags_management\CtmRuleListBuilder",
 *     "form" = {
 *       "add" = "Drupal\cache_tags_management\Form\CtmRuleForm",
 *       "edit" = "Drupal\cache_tags_management\Form\CtmRuleForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "ctm_rule",
 *   admin_permission = "administer ctm_rule",
 *   links = {
 *     "collection" = "/admin/structure/ctm-rule",
 *     "add-form" = "/admin/structure/ctm-rule/add",
 *     "edit-form" = "/admin/structure/ctm-rule/{ctm_rule}",
 *     "delete-form" = "/admin/structure/ctm-rule/{ctm_rule}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "weight" = "weight",
 *     "status" = "status"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "status",
 *     "weight",
 *     "plugin",
 *     "configuration",
 *     "conditions",
 *     "conditionOperator"
 *   }
 * )
 */
class CtmRule extends ConfigEntityBase implements CtmRuleInterface, EntityWithPluginCollectionInterface {

  /**
   * The ctm rule ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The ctm rule label.
   *
   * @var string
   */
  protected $label;

  /**
   * The ctm rule status.
   *
   * @var bool
   */
  protected $status;

  /**
   * The ctm_rule description.
   *
   * @var string
   */
  protected $description;

  /**
   * The payment gateway weight.
   *
   * @var int
   */
  protected $weight;

  /**
   * The plugin ID.
   *
   * @var string
   */
  protected $plugin;

  /**
   * The plugin configuration.
   *
   * @var array
   */
  protected $configuration = [];

  /**
   * The insertion conditions.
   *
   * Each item is the configuration array not the condition object.
   *
   * @var array
   */
  protected $conditions = [];

  /**
   * The insertion condition collection.
   *
   * @var \Drupal\Core\Condition\ConditionPluginCollection
   */
  protected $conditionCollection;

  /**
   * The condition plugin manager.
   *
   * @var \Drupal\Core\Executable\ExecutableManagerInterface
   */
  protected $conditionPluginManager;

  /**
   * The condition operator.
   *
   * @var string
   */
  protected $conditionOperator = 'AND';

  /**
   * Sets the configuration for an insertion condition.
   *
   * @param string $instance_id
   *   The condition instance ID.
   * @param array $configuration
   *   The condition configuration.
   *
   * @return $this
   *
   * @todo Does this need to set a persistent property?
   */
  public function setCondition($instance_id, array $configuration) {
    $conditions = $this->getConditions();
    if (!$conditions->has($instance_id)) {
      $configuration['id'] = $instance_id;
      $conditions->addInstanceId($instance_id, $configuration);
    }
    else {
      $conditions->setInstanceConfiguration($instance_id, $configuration);
    }
    return $this;
  }

  /**
   * Returns the set of insertion conditions for this container.
   *
   * @return \Drupal\Core\Condition\ConditionPluginCollection
   *   A collection of configured condition plugins.
   */
  public function getConditions() {
    if (!isset($this->conditionCollection)) {
      $this->conditionCollection = new ConditionPluginCollection($this->conditionPluginManager(), $this->get('conditions'));
    }
    return $this->conditionCollection;
  }

  /**
   * Gets the condition plugin manager.
   *
   * @return \Drupal\Core\Executable\ExecutableManagerInterface
   *   The condition plugin manager.
   */
  protected function conditionPluginManager() {
    if (!isset($this->conditionPluginManager)) {
      $this->conditionPluginManager = \Drupal::service('plugin.manager.condition');
    }
    return $this->conditionPluginManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginCollections() {
    return [
      'conditions' => $this->getConditions(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConditionOperator(bool $lower = FALSE) {
    if ($lower) {
      return strtolower($this->conditionOperator);
    }
    return $this->conditionOperator;
  }

  /**
   * {@inheritdoc}
   */
  public function setConditionOperator($condition_operator) {
    $this->conditionOperator = $condition_operator;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPlugin() {
    return $this->plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginInstance() {
    $manager = \Drupal::service('plugin.manager.ctm_action');
    return $manager->createInstance($this->plugin, $this->configuration[$this->plugin]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigurations() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus() {
    return $this->status;
  }

}
