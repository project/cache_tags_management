<?php

namespace Drupal\cache_tags_management\Form;

use Drupal\Core\Condition\ConditionInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Executable\ExecutableManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * CTM Rule form.
 *
 * @property \Drupal\cache_tags_management\CtmRuleInterface $entity
 */
class CtmRuleForm extends EntityForm {

  /**
   * The condition plugin manager.
   *
   * @var \Drupal\Core\Condition\ConditionManager
   */
  protected $conditionManager;

  /**
   * The context repository service.
   *
   * @var \Drupal\Core\Plugin\Context\ContextRepositoryInterface
   */
  protected $contextRepository;

  /**
   * Constructs a ContainerForm object.
   *
   * @param \Drupal\Core\Executable\ExecutableManagerInterface $condition_manager
   *   The ConditionManager for building the insertion conditions.
   * @param \Drupal\Core\Plugin\Context\ContextRepositoryInterface $context_repository
   *   The lazy context repository service.
   */
  public function __construct(ExecutableManagerInterface $condition_manager, ContextRepositoryInterface $context_repository) {
    $this->conditionManager = $condition_manager;
    $this->contextRepository = $context_repository;
  }

  /**
   * {@inheritdoc}
   *
   * This routine is the trick to DependencyInjection in Drupal. Without it the
   * __construct method complains of no arguments instead of three.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.condition'),
      $container->get('context.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    $form = parent::form($form, $form_state);

    // Store the contexts for other objects to use during form building.
    $form_state->setTemporaryValue('gathered_contexts', $this->contextRepository->getAvailableContexts());

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#description' => $this->t('Label for the ctm rule.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\cache_tags_management\Entity\CtmRule::load',
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->entity->status(),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $this->entity->get('description'),
      '#description' => $this->t('Description of the ctm rule.'),
    ];

    $manager = \Drupal::service('plugin.manager.ctm_action');
    $plugins = [];
    $definitions = $manager->getDefinitions();
    $plugins_forms = [];
    $configuration = $this->entity->getConfigurations();
    foreach ($definitions as $id => $def) {
      $conf = !empty($configuration[$id]) ? $configuration[$id] : [];
      /** @var \Drupal\cache_tags_management\CtmActionInterface $instance */
      $instance = $manager->createInstance($id, $conf);
      $plugins[$id] = $instance->label();
      $plugins_forms[$id] = ['#type' => 'container'] + $instance->buildConfigurationForm($conf, $form_state);
      $plugins_forms[$id]['#states']['visible'][':input[name="plugin"]'] = ['value' => $id];
    }
    $form['plugin'] = [
      '#type' => 'select',
      '#title' => $this->t('Action'),
      '#options' => $plugins,
      '#default_value' => $this->entity->getPlugin(),
      '#required' => TRUE,
    ];

    $form['configuration'] = [
      '#type' => 'container',
      '#tree' => TRUE,
    ] + $plugins_forms;

    $form['conditions'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Insertion conditions'),
      '#description' => $this->t('The snippet insertion conditions for this container.'),
      '#attributes' => ['class' => ['ctm-rule']],
    ];

    $form += $this->conditionsForm([], $form_state);

    $form['conditionOperator'] = [
      '#type' => 'radios',
      '#title' => $this->t('Condition operator'),
      '#title_display' => 'invisible',
      '#options' => [
        'AND' => $this->t('All conditions must pass'),
        'OR' => $this->t('Only one condition must pass'),
      ],
      '#default_value' => $this->entity->getConditionOperator(),
    ];

    return $form;
  }

  /**
   * Builds the form elements for the insertion conditions.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The augmented form array with the insertion condition elements.
   */
  protected function conditionsForm(array $form, FormStateInterface $form_state) {
    $conditions = $this->entity->getConditions();
    // See core/lib/Drupal/Core/Plugin/FilteredPluginManagerTrait.php
    // The next method calls alter hooks to filter the definitions.
    // Implement one of the hooks in this module.
    $definitions = $this->conditionManager->getFilteredDefinitions('cache_tags_management', $form_state->getTemporaryValue('gathered_contexts'), ['ctm_rule' => $this->entity]);
    ksort($definitions);
    $form_state->setTemporaryValue('filtered_conditions', array_keys($definitions));
    foreach ($definitions as $condition_id => $definition) {
      if ($conditions->has($condition_id)) {
        $condition = $conditions->get($condition_id);
      }
      else {
        /** @var \Drupal\Core\Condition\ConditionInterface $condition */
        $condition = $this->conditionManager->createInstance($condition_id, []);
      }
      $form_state->set(['conditions', $condition_id], $condition);
      $form[$condition_id] = $this->conditionFieldset($condition, $form_state);
    }

    return $form;
  }

  /**
   * Returns the form elements from the condition plugin object.
   *
   * @param \Drupal\Core\Condition\ConditionInterface $condition
   *   The condition plugin.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form array for the insertion condition.
   */
  public function conditionFieldset(ConditionInterface $condition, FormStateInterface $form_state) {
    // Build form elements.
    $fieldset = [
      '#type' => 'details',
      '#title' => $condition->getPluginDefinition()['label'],
      '#group' => 'conditions',
      '#tree' => TRUE,
    ] + $condition->buildConfigurationForm([], $form_state);

    return $fieldset;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $this->validateConditionsForm($form, $form_state);
  }

  /**
   * Form validation handler for the insertion conditions.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  protected function validateConditionsForm(array $form, FormStateInterface $form_state) {
    // Validate the insertion condition settings.
    $condition_ids = $form_state->getTemporaryValue('filtered_conditions');
    foreach ($condition_ids as $condition_id) {
      // Allow the condition to validate the form.
      $condition = $form_state->get(['conditions', $condition_id]);
      $condition->validateConfigurationForm($form[$condition_id], SubformState::createForSubform($form[$condition_id], $form, $form_state));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->submitConditionsForm($form, $form_state);
  }

  /**
   * Form submission handler for the insertion conditions.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  protected function submitConditionsForm(array $form, FormStateInterface $form_state) {
    $condition_ids = $form_state->getTemporaryValue('filtered_conditions');
    foreach ($condition_ids as $condition_id) {
      $values = $form_state->getValue($condition_id);
      // Allow the condition to submit the form.
      $condition = $form_state->get(['conditions', $condition_id]);
      $condition->submitConfigurationForm($form[$condition_id], SubformState::createForSubform($form[$condition_id], $form, $form_state));
      $configuration = $condition->getConfiguration();
      // Update the insertion conditions on the container.
      $this->entity->setCondition($condition_id, $configuration);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);
    $message_args = ['%label' => $this->entity->label()];
    $message = $result == SAVED_NEW
      ? $this->t('Created new ctm rule %label.', $message_args)
      : $this->t('Updated ctm rule %label.', $message_args);
    $this->messenger()->addStatus($message);
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

}
