<?php

namespace Drupal\cache_tags_management\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines ctm_action annotation object.
 *
 * @Annotation
 */
class CtmAction extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

}
