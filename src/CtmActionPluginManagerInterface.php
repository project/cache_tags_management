<?php

namespace Drupal\cache_tags_management;

/**
 * Interface for the CtmActionPluginManager.
 */
interface CtmActionPluginManagerInterface {

  /**
   * Get all available actions.
   *
   * @return array
   *   An array of actions.
   */
  public function getActions();

  /**
   * Get an action by ID.
   *
   * @param string $id
   *   The action ID.
   *
   * @return \Drupal\cache_tags_management\CtmActionInterface
   *   The action object.
   */
  public function getAction($id);

}
