<?php

namespace Drupal\cache_tags_management\Plugin\CtmAction;

use Drupal\cache_tags_management\CtmActionPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Plugin implementation of the ctm_action.
 *
 * @CtmAction(
 *   id = "remove_tag",
 *   label = @Translation("Remove Tag"),
 *   description = @Translation("Remove a tag from the cache.")
 * )
 */
class RemoveTag extends CtmActionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function execute(Response $response) {
    $response_cacheability = $response->getCacheableMetadata();
    $cache_tags = $response_cacheability->getCacheTags();
    $black_list = $this->configuration['tags'] ? explode("\r\n", $this->configuration['tags']) : [];
    foreach ($black_list as $tag) {
      if ($pos = array_search($tag, $cache_tags)) {
        unset($cache_tags[$pos]);
      }
    }

    $response_cacheability->setCacheTags($cache_tags);
    $response->addCacheableDependency($response_cacheability);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $config, FormStateInterface $form_state) {
    $form['tags'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Tags'),
      '#description' => $this->t('One tag per line.'),
      '#required' => TRUE,
      '#default_value' => $config['tags'] ?? '',
    ];
    return $form;
  }

}
