<?php

/**
 * @file
 */

/**
 * Implements hook_views_data().
 */
function cache_tags_monitor_views_data() {
  $data = [];
  $data['cachemonitor_items']['table']['group'] = t('Cache Tags Monitor');
  $data['cachemonitor_items']['table']['base'] = [
    'field' => 'id',
    'title' => t('Cache Tags Monitor'),
    'help' => t('Cache Tags Monitor'),
    'weight' => -10,
  ];

  $data['cachemonitor_items']['id'] = [
    'title' => t('ID'),
    'help' => t('The ID of the cache item.'),
    'field' => [
      'id' => 'numeric',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['cachemonitor_items']['uid'] = [
    'title' => t('User ID'),
    'help' => t('The user ID of the cache item.'),
    'relationship' => [
      'title' => t('User'),
      'help' => t('The user on which the log entry as written.'),
      'base' => 'users_field_data',
      'base field' => 'uid',
      'id' => 'standard',
    ],
    'field' => [
      'id' => 'numeric',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['cachemonitor_items']['invalidator'] = [
    'title' => t('Invalidator'),
    'help' => t('The invalidator of the cache item.'),
    'relationship' => [
      'title' => t('Invalidator Tag'),
      'help' => t('The invalidator tag.'),
      'base' => 'cachemonitor_invalidator',
      'base field' => 'id',
      'label' => t('Invalidator'),
      'id' => 'standard',
    ],
    'field' => [
      'id' => 'numeric',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['cachemonitor_items']['expired'] = [
    'title' => t('Expired'),
    'help' => t('The expired of the cache item.'),
    'field' => [
      'id' => 'boolean',
    ],
    'filter' => [
      'id' => 'boolean',
    ],
    'sort' => [
      'id' => 'numeric',
    ],
  ];

  $data['cachemonitor_items']['url'] = [
    'title' => t('URL'),
    'help' => t('The URL of the cache item.'),
    'field' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'string',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['cachemonitor_items']['tags'] = [
    'title' => t('Tags'),
    'help' => t('The tags of the cache item.'),
    'field' => [
      'id' => 'tags',
    ],
    'filter' => [
      'id' => 'string',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['cachemonitor_items']['context'] = [
    'title' => t('Cache Context'),
    'help' => t('The cache context of the cache item.'),
    'field' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'string',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['cachemonitor_items']['session_id'] = [
    'title' => t('Session ID'),
    'help' => t('The session ID of the cache item.'),
    'field' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'standard',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['cachemonitor_items']['created'] = [
    'title' => t('Created'),
    'help' => t('The created of the cache item.'),
    'field' => [
      'id' => 'date',
    ],
    'sort' => [
      'id' => 'date',
    ],
    'filter' => [
      'id' => 'date',
    ],
  ];

  $data['cachemonitor_items']['invalidated'] = [
    'title' => t('Invalidated'),
    'help' => t('The invalidated of the cache item.'),
    'field' => [
      'id' => 'date',
    ],
    'sort' => [
      'id' => 'date',
    ],
    'filter' => [
      'id' => 'date',
    ],
  ];

  $data['cachemonitor_items']['lifetime'] = [
    'title' => t('Life Time'),
    'help' => t('The life time of the cache item.'),
    'field' => [
      'id' => 'duration',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['cachemonitor_items']['data'] = [
    'title' => t('Data'),
    'help' => t('The data of the cache item.'),
    'field' => [
      'id' => 'serialized',
    ],
    'filter' => [
      'id' => 'standard',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['cachemonitor_invalidator']['table']['group'] = t('Cache Tags Monitor Invalidator');
  $data['cachemonitor_invalidator']['table']['base'] = [
    'field' => 'id',
    'title' => t('Cache Tags Monitor Invalidator'),
    'help' => t('Cache Tags Monitor Invalidator'),
    'weight' => -10,
  ];

  $data['cachemonitor_invalidator']['id'] = [
    'title' => t('ID'),
    'help' => t('The ID of the invalidator.'),
    'field' => [
      'id' => 'numeric',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['cachemonitor_invalidator']['uid'] = [
    'title' => t('User ID'),
    'help' => t('The user ID of the invalidator.'),
    'relationship' => [
      'title' => t('User'),
      'help' => t('The user on which the log entry as written.'),
      'base' => 'users_field_data',
      'base field' => 'uid',
      'id' => 'standard',
    ],
    'field' => [
      'id' => 'numeric',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['cachemonitor_invalidator']['url'] = [
    'title' => t('URL'),
    'help' => t('The URL of the invalidator.'),
    'field' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'string',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['cachemonitor_invalidator']['tag'] = [
    'title' => t('Tag'),
    'help' => t('The tag of the invalidator.'),
    'field' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'string',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['cachemonitor_invalidator']['created'] = [
    'title' => t('Created'),
    'help' => t('The created of the invalidator.'),
    'field' => [
      'id' => 'date',
    ],
    'sort' => [
      'id' => 'date',
    ],
    'filter' => [
      'id' => 'date',
    ],
  ];

  $data['cachemonitor_invalidator']['data'] = [
    'title' => t('Data'),
    'help' => t('The data of the invalidator.'),
    'field' => [
      'id' => 'serialized',
    ],
    'filter' => [
      'id' => 'standard',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  return $data;
}

/**
 * Implements hook_views_data_alter().
 */
function cache_tags_monitor_views_data_alter(array &$data) {
  $data['views']['info'] = [
    'title' => t('+Info'),
    'help' => t('Display fields in a details element.'),
    'field' => [
      'id' => 'info',
    ],
  ];
}
