<?php

namespace Drupal\cache_tags_monitor\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Provides Duration field handler.
 *
 * @ViewsField("duration")
 *
 * @DCG
 * The plugin needs to be assigned to a specific table column through
 * hook_views_data() or hook_views_data_alter().
 * For non-existent columns (i.e. computed fields) you need to override
 * self::query() method.
 */
class Duration extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $value = $this->getValue($values);
    if ($value == 0) {
      return $this->sanitizeValue('-');
    }
    $v = gmdate("H:i:s", $value);
    return $this->sanitizeValue($v);
  }

}
