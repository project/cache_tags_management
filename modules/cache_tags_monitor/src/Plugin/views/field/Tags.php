<?php

namespace Drupal\cache_tags_monitor\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Provides Tags field handler.
 *
 * @ViewsField("tags")
 *
 * @DCG
 * The plugin needs to be assigned to a specific table column through
 * hook_views_data() or hook_views_data_alter().
 * For non-existent columns (i.e. computed fields) you need to override
 * self::query() method.
 */
class Tags extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $value = $this->getValue($values);
    $result = [
      '#type' => 'details',
      '#title' => $this->t('Tags'),
      '#open' => FALSE,
      'tags' => [
        '#type' => 'markup',
        '#markup' => $value,
      ],
    ];
    return $result;
  }

}
