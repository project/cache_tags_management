<?php

namespace Drupal\cache_tags_monitor\Plugin\views\field;

use Drupal\views\Plugin\views\field\Links;
use Drupal\views\ResultRow;

/**
 * Provides a handler that renders items as details.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("info")
 */
class Info extends Links {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $items = $this->getItems();

    if (!empty($items)) {
      return [
        '#type' => 'details',
        '#title' => $this->t($this->label()),
        '#open' => FALSE,
        'items' => [
          '#theme' => 'item_list',
          '#items' => array_values($items),
        ],
      ];
    }
    else {
      return '';
    }
  }

  /**
   * Gets the list of links used by this field.
   *
   * @return array
   *   The links which are used by the render function.
   */
  protected function getItems() {
    $links = [];
    foreach ($this->options['fields'] as $field) {
      if (empty($this->view->field[$field]->last_render_text)) {
        continue;
      }
      $label = $this->view->field[$field]->label();
      $title = $this->view->field[$field]->last_render_text;
      $links[$field] = [
        '#type' => 'markup',
        '#markup' => $this->sanitizeValue("{$label}: $title"),
      ];
    }

    return $links;
  }

}
