<?php

namespace Drupal\cache_tags_monitor;

use Drupal\Core\Cache\CacheTagsChecksumTrait;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\State\StateInterface;
use GuzzleHttp\ClientInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class provides a set of Varnish cache operations.
 */
class CacheTagsMonitor implements CacheTagsInvalidatorInterface {

  use CacheTagsChecksumTrait;

  /**
   * GuzzleHttp\ClientInterface definition.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * A config factory for retrieving required config objects.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Drupal request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Drupal response.
   *
   * @var \Symfony\Component\HttpFoundation\Response
   */
  protected $response;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * User account interface.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $account;

  /**
   * The current path.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * The path matcher.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * The route admin context to determine whether a route is an admin one.
   *
   * @var \Drupal\Core\Routing\AdminContext
   */
  protected $adminContext;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Stores the state storage service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The kill switch.
   *
   * @var \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   */
  protected $killSwitch;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs a new CacheManager object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   Http service for requests.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   Request stack service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   The current account.
   * @param \Drupal\Core\Path\CurrentPathStack $current_path
   *   The current path.
   * @param \Drupal\Core\Path\PathMatcherInterface $path_matcher
   *   The path matcher service.
   * @param \Drupal\Core\Routing\AdminContext $admin_context
   *   The route admin context to determine whether the route is an admin one.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state key value store.
   * @param \Drupal\Core\PageCache\ResponsePolicy\KillSwitch $killSwitch
   *   The kill switch.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(
    ClientInterface $http_client,
    ConfigFactoryInterface $config_factory,
    RequestStack $request,
    RouteMatchInterface $route_match,
    AccountProxyInterface $account,
    CurrentPathStack $current_path,
    PathMatcherInterface $path_matcher,
    AdminContext $admin_context,
    StateInterface $state,
    KillSwitch $killSwitch,
    Connection $connection,
  ) {
    $this->httpClient = $http_client;
    // $this->config = $config_factory->get('adv_varnish.cache_settings');
    $this->request = $request->getCurrentRequest();
    $this->routeMatch = $route_match;
    $this->account = $account;
    $this->currentPath = $current_path;
    $this->pathMatcher = $path_matcher;
    $this->adminContext = $admin_context;
    $this->state = $state;
    $this->killSwitch = $killSwitch;
    $this->connection = $connection;
  }

  /**
   * Gets the request object.
   *
   * @return \Symfony\Component\HttpFoundation\Request
   *   The request object.
   */
  protected function getRequest() {
    if (!$this->request) {
      /* @phpstan-ignore-next-line */
      $this->request = \Drupal::request();
    }
    return $this->request;
  }

  /**
   * Get specified condition operator mapping value.
   *
   * @param string $operator
   *   Condition operator.
   *
   * @return string
   *   Specified condition operator mapping value.
   */
  protected function getConditionOperator($operator) {
    $mapping = $this->connection->mapConditionOperator($operator);
    return $mapping['operator'] ?? $operator;
  }

  /**
   * {@inheritdoc}
   */
  public function doInvalidateTags(array $tags) {
    try {
      $operator = $this->getConditionOperator('LIKE');
      $no_effect = [];
      foreach ($tags as $tag) {
        $created = time();
        $id = $this->connection->insert('cachemonitor_invalidator')
          ->fields([
            'tag' => $tag,
            'uid' => $this->account->id(),
            'created' => $created,
            'url' => $this->getRequest()->getUri(),
          ])
          ->execute();

        $query = $this->connection->update('cachemonitor_items');
        $or = $query->orConditionGroup()
          ->condition('tags', '%|' . $tag . '|%', $operator)
          ->condition('tags', $tag . '|%', $operator)
          ->condition('tags', '%|' . $tag, $operator);
        $query->fields([
          'invalidator' => $id,
          'invalidated' => $created,
          'expired' => 1,
        ])
          ->expression('lifetime', $created . ' - [created]')
          ->condition($or)
          ->condition('expired', 0);
        if (!$query->execute()) {
          $no_effect[] = $id;
        }
      }
      $this->connection->delete('cachemonitor_invalidator')
        ->condition('id', $no_effect, 'IN')
        ->execute();
    }
    catch (\Exception $e) {

    }
  }

  /**
   * {@inheritdoc}
   */
  public function getDatabaseConnection() {
    return $this->connection;
  }

  /**
   * {@inheritdoc}
   */
  protected function getTagInvalidationCounts(array $tags) {
    return [];
  }

}
