<?php

namespace Drupal\cache_tags_monitor\EventSubscriber;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\Cache\Context\CacheContextsManager;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\PageCache\RequestPolicyInterface;
use Drupal\Core\PageCache\ResponsePolicyInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Site\Settings;
use Drupal\dynamic_page_cache\EventSubscriber\DynamicPageCacheSubscriber;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Response subscriber to handle finished responses.
 */
class FinishResponseSubscriber implements EventSubscriberInterface {

  /**
   * The language manager object for retrieving the correct language code.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * A config object for the system performance configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * A policy rule determining the cacheability of a request.
   *
   * @var \Drupal\Core\PageCache\RequestPolicyInterface
   */
  protected $requestPolicy;

  /**
   * A policy rule determining the cacheability of the response.
   *
   * @var \Drupal\Core\PageCache\ResponsePolicyInterface
   */
  protected $responsePolicy;

  /**
   * The cache contexts manager service.
   *
   * @var \Drupal\Core\Cache\Context\CacheContextsManager
   */
  protected $cacheContextsManager;

  /**
   * User account interface.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $account;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;


  /**
   * The renderer configuration.
   *
   * @var array
   */
  protected $rendererConfig;

  /**
   * Constructs a FinishResponseSubscriber object.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager object for retrieving the correct language code.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A config factory for retrieving required config objects.
   * @param \Drupal\Core\PageCache\RequestPolicyInterface $request_policy
   *   A policy rule determining the cacheability of a request.
   * @param \Drupal\Core\PageCache\ResponsePolicyInterface $response_policy
   *   A policy rule determining the cacheability of a response.
   * @param \Drupal\Core\Cache\Context\CacheContextsManager $cache_contexts_manager
   *   The cache contexts manager service.
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   The current account.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Extension\ModuleHandler $module_handler
   *   The module handler.
   * @param array $renderer_config
   *   The renderer configuration.
   */
  public function __construct(LanguageManagerInterface $language_manager, ConfigFactoryInterface $config_factory, RequestPolicyInterface $request_policy, ResponsePolicyInterface $response_policy, CacheContextsManager $cache_contexts_manager, AccountProxyInterface $account, Connection $connection, ModuleHandlerInterface $module_handler, array $renderer_config) {
    $this->languageManager = $language_manager;
    $this->config = $config_factory->get('system.performance');
    $this->requestPolicy = $request_policy;
    $this->responsePolicy = $response_policy;
    $this->cacheContextsManager = $cache_contexts_manager;
    $this->account = $account;
    $this->connection = $connection;
    $this->moduleHandler = $module_handler;
    $this->rendererConfig = $renderer_config;
  }

  /**
   * Registers the methods in this class that should be listeners.
   *
   * @return array
   *   An array of event listener definitions.
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = ['onRespond'];
    $events[KernelEvents::RESPONSE][] = ['onDynamicRespond', 100];
    return $events;
  }

  /**
   * Logs the cache contexts and cache tags associated with this page.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The event to process.
   */
  public function onDynamicRespond(ResponseEvent $event) {
    if (!$event->isMainRequest()) {
      return;
    }

    $request = $event->getRequest();
    $response = $event->getResponse();

    if (!$response instanceof CacheableResponseInterface) {
      return;
    }

    // if($response->headers->get('X-Drupal-Cache') === 'HIT'){
    //      return;
    //    }.
    // There's no work left to be done if this is a Dynamic Page Cache hit.
    if ($this->moduleHandler->moduleExists('dynamic_page_cache') &&
      $response->headers->get(DynamicPageCacheSubscriber::HEADER) === 'HIT') {
      return;
    }

    // There's no work left to be done if this is an uncacheable response.
    if (!$this->shouldCacheResponse($response)) {
      return;
    }

    $this->log($request, $response);
  }

  /**
   * Whether the given response should be cached by Dynamic Page Cache.
   *
   * We consider any response that has cacheability metadata meeting the auto-
   * placeholdering conditions to be uncacheable. Because those conditions
   * indicate poor cacheability, and if it doesn't make sense to cache parts of
   * a page, then neither does it make sense to cache an entire page.
   *
   * But note that auto-placeholdering avoids such cacheability metadata ever
   * bubbling to the response level: while rendering, the Renderer checks every
   * subtree to see if meets the auto-placeholdering conditions. If it does, it
   * is automatically placeholdered, and consequently the cacheability metadata
   * of the placeholdered content does not bubble up to the response level.
   *
   * @param \Drupal\Core\Cache\CacheableResponseInterface $response
   *   The response whose cacheability to analyze.
   *
   * @return bool
   *   Whether the given response should be cached.
   *
   * @see \Drupal\Core\Render\Renderer::shouldAutomaticallyPlaceholder()
   */
  protected function shouldCacheResponse(CacheableResponseInterface $response) {
    $conditions = $this->rendererConfig['auto_placeholder_conditions'];

    $cacheability = $response->getCacheableMetadata();

    // Response's max-age is at or below the configured threshold.
    if ($cacheability->getCacheMaxAge() !== Cache::PERMANENT && $cacheability->getCacheMaxAge() <= $conditions['max-age']) {
      return FALSE;
    }

    // Response has a high-cardinality cache context.
    if (array_intersect($cacheability->getCacheContexts(), $conditions['contexts'])) {
      return FALSE;
    }

    // Response has a high-invalidation frequency cache tag.
    if (array_intersect($cacheability->getCacheTags(), $conditions['tags'])) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Log the cache contexts and cache tags associated with this page.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   * @param \Symfony\Component\HttpFoundation\Response $response
   *   The response object.
   */
  protected function log(Request $request, Response $response) {
    $response_cacheability = $response->getCacheableMetadata();
    $max_age_message = $response_cacheability->getCacheMaxAge();
    if ($max_age_message === 0) {
      return;
    }

    $cache_tags = $response_cacheability->getCacheTags();
    if (empty($cache_tags)) {
      return;
    }
    sort($cache_tags);
    $tags = implode('|', $cache_tags);
    $cache_contexts = $this->cacheContextsManager->optimizeTokens($response_cacheability->getCacheContexts());
    sort($cache_contexts);
    $context = implode(' ', $cache_contexts);
    $created = time();
    $id = $this->connection->insert('cachemonitor_items')
      ->fields([
        'uid' => $this->account->id(),
        'tags' => $tags,
        'context' => $context,
        'created' => $created,
        'session_id' => session_id(),
        'url' => $request->getUri(),
      ])
      ->execute();
  }

  /**
   * Sets extra headers on successful responses.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The event to process.
   */
  public function onRespond(ResponseEvent $event) {
    if (!$event->isMainRequest()) {
      return;
    }

    $request = $event->getRequest();
    $response = $event->getResponse();

    if (!$response instanceof CacheableResponseInterface) {
      return;
    }

    // There's no work left to be done if this is a Dynamic Page Cache hit.
    if ($this->moduleHandler->moduleExists('dynamic_page_cache') &&
      $response->headers->get(DynamicPageCacheSubscriber::HEADER) === 'HIT') {
      return;
    }

    if (!$this->cacheable($request, $response)) {
      return;
    }

    $this->log($request, $response);
  }

  /**
   * Determine if the response is cacheable.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request object.
   * @param \Symfony\Component\HttpFoundation\Response $response
   *   A response object that should be stored in the page cache.
   *
   * @returns bool
   */
  protected function cacheable(Request $request, Response $response) {
    if (!$response instanceof CacheableResponseInterface) {
      return FALSE;
    }

    if ($response instanceof BinaryFileResponse || $response instanceof StreamedResponse) {
      return FALSE;
    }

    // Allow policy rules to further restrict which responses to cache.
    if ($this->responsePolicy->check($response, $request) === ResponsePolicyInterface::DENY) {
      return FALSE;
    }

    $request_time = $request->server->get('REQUEST_TIME');
    $expire = 0;
    if ($response->isClientError()) {
      $cache_ttl_4xx = Settings::get('cache_ttl_4xx', 3600);
      if ($cache_ttl_4xx > 0) {
        $expire = $request_time + $cache_ttl_4xx;
      }
    }
    elseif ($expires = $response->getExpires()) {
      $date = $expires->getTimestamp();
      $expire = ($date > $request_time) ? $date : Cache::PERMANENT;
    }
    else {
      $expire = Cache::PERMANENT;
    }

    if ($expire === Cache::PERMANENT || $expire > $request_time) {
      return TRUE;
    }

    return FALSE;
  }

}
